package com.fzsyw.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

class ClientSocket {

	private ExecutorService executorService;
	
	{
		ThreadFactory threadFactory = new ThreadFactory() {
			
			@Override
			public Thread newThread(Runnable r) {
				Thread th = new Thread(r);
				th.setDaemon(true);
				return th;
			}
		};
		executorService = Executors.newSingleThreadExecutor(threadFactory);
	}
	
	public Object transfer(Object obj) {
		Socket socket = new Socket();
		try{
			socket.bind(new InetSocketAddress("localhost", 7777));
			socket.connect(new InetSocketAddress("localhost", 9999));
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(obj);
			oos.flush();
			

			return getResult(socket);
		} catch (Exception e) {			
			e.printStackTrace();
			throw new RuntimeException(e);
		}finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private Object getResult(Socket socket) throws Exception {
		Callable<Object> task = ()->{
			while(true) {
				int size = socket.getInputStream().available();
				if(size != 0) {
					ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
					return ois.readObject();
				}
			}
		};
		
		Future<Object> future = executorService.submit(task);
		return future.get(2, TimeUnit.SECONDS);
	}
	
}
