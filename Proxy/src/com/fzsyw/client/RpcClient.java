package com.fzsyw.client;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import com.fzsyw.domain.TransferMessage;

public class RpcClient implements InvocationHandler {

	@SuppressWarnings("unchecked")
	public static <T> T proxy(Class<T> clazz) {
		return (T) Proxy.newProxyInstance(RpcClientTest.class.getClassLoader(),
			new Class[] {clazz}, new RpcClient(clazz));
	}
	
	private Class<?> clazz;
	
	protected RpcClient(Class<?> clazz) {
		this.clazz = clazz;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		TransferMessage transferMessage = new TransferMessage(clazz.getName(), 
			method.getName(), args);
		return new ClientSocket().transfer(transferMessage);
	}

}
