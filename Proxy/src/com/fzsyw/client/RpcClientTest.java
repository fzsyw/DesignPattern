package com.fzsyw.client;

import com.fzsyw.domain.ITest;
import com.fzsyw.domain.Person;

public class RpcClientTest {

	
	public static void main(String[] args) {
		ITest test = RpcClient.proxy(ITest.class);
		System.out.println(test.say("hello", new Person("DD", 34)));
	}
	
}
