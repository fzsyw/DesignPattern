package com.fzsyw.domain;

public interface ITest {

	String say(String name);

	String say(String name, Person person);

}
