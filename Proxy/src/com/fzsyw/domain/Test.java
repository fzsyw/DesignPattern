package com.fzsyw.domain;

public class Test implements ITest{

	@Override
	public String say(String msg) {
		return msg+": ss";
	}
	
	@Override
	public String say(String msg, Person person) {
		return person.getName()+": age is " +person.getAge()+" "+msg;
	}
}
