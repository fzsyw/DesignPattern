package com.fzsyw.domain;

import java.io.Serializable;

public class TransferMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String className;
	
	private String methodName;
	
	private Object[] objs;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Object[] getObjs() {
		return objs;
	}

	public void setObjs(Object[] objs) {
		this.objs = objs;
	}

	public TransferMessage(String className, String methodName, Object[] objs) {
		super();
		this.className = className;
		this.methodName = methodName;
		this.objs = objs;
	}

	public TransferMessage() {}
}
