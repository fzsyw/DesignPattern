package com.fzsyw.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import com.fzsyw.domain.TransferMessage;

public class RpcServer {

	private ServerSocket serverSocket;
	
	private Map<String, Object> map;
	private ExecutorService executorService;
	
	
	public RpcServer() throws Exception{
		map = new HashMap<>();	
		serverSocket = new ServerSocket(9999);
		executorService = Executors.newCachedThreadPool();
	}

	
	public void init() throws IOException{		
		while(true) {
			
			Socket socket = serverSocket.accept();
			executorService.submit(()->{
				try {
					ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
					TransferMessage	transferMessage =  (TransferMessage) ois.readObject();
					
					Object result = calculate(transferMessage);			
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject(result);
					oos.flush();
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}finally {
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}				
			});		
		}	
	}
	
	
	@SuppressWarnings("rawtypes")
	private Object calculate(TransferMessage transferMessage) throws Exception {
		Object obj = map.get(transferMessage.getClassName());
		Object[] objs = transferMessage.getObjs();
		Object result = null;
		if(objs == null || objs.length == 0) {
			Method method = obj.getClass().getMethod(transferMessage.getMethodName());
			result = method.invoke(obj);
		}else {
			List<Class<?>> list = Arrays.stream(objs).map(p->p.getClass()).collect(Collectors.toList());
			Class[] clazzs = new Class[objs.length];
			list.toArray(clazzs);
			Method method = obj.getClass().getMethod(transferMessage.getMethodName(), clazzs);
			result = method.invoke(obj, objs);
		}
		return result;
	}
	
	public void register(Class<?> interClass, Object obj) {
		map.put(interClass.getName(), obj);
	} 
	
}
