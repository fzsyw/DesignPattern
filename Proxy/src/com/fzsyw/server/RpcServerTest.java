package com.fzsyw.server;

import com.fzsyw.domain.ITest;
import com.fzsyw.domain.Test;

public class RpcServerTest {

	public static void main(String[] args) throws Exception {
		RpcServer rpcServer = new RpcServer();
		rpcServer.register(ITest.class, new Test());
		rpcServer.init();
	}
}
